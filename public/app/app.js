var alt = angular.module('alt', ['ngResource', 'ngRoute', 'firebase', 'restangular']);

alt.constant('FIREBASE_URL', 'https://alovelything.firebaseio.com');

alt
.config(function($routeProvider, $locationProvider, RestangularProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  RestangularProvider.setBaseUrl('/data');
  $routeProvider
  .when('/', { templateUrl: '/pages/index' })
  .when('/brand/inspirations', { templateUrl: '/pages/brand/brand-Inspirations' });
});