alt.factory('brand', function($rootScope, FIREBASE_URL, $firebaseArray, $firebaseObject) {
  var inspirationsRef = new Firebase(FIREBASE_URL + '/inspirations'); 
  output = {
    getBrandInspirations: function(brand) {
      return $firebaseArray(inspirationsRef.orderByChild('brand').equalTo(brand))
    }
  }

  return output
});